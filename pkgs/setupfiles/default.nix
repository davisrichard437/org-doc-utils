{ pkgs ? import <nixpkgs> { }, stdenv ? pkgs.stdenvNoCC }:

stdenv.mkDerivation rec {
  pname = "setupfiles";
  version = "0.1.0";

  src = ./.;

  builder = with pkgs; writeShellScript "builder.sh" ''
    export PATH=${coreutils}/bin
    cd $src
    mkdir -p $out/share/setupfiles
    cp -r ./setupfiles $out/share/
  '';
}
