#!/usr/bin/env -S emacs -Q --script

;; publish.el - emacs script to publish website
;; Copyright (C) 2022 Richard Davis

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(require 'ox-publish)

(setq build-dir "./build"
      src-dir "./src"
      org-latex-pdf-process '("lualatex %F"))

(setq org-publish-project-alist
      (list
       (list "doc"
             :recursive t
             :base-directory src-dir
             :publishing-directory build-dir
             :publishing-function #'org-latex-publish-to-pdf)))

(condition-case nil
    (progn
      (delete-directory build-dir t)
      (mkdir build-dir)
      (org-publish-all t))
  (error nil))

(message "Build complete!")
