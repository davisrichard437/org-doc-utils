{
  description = "Utilities for handling emacs org-mode pdf documents.";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    with flake-utils.lib; eachSystem allSystems
      (system:
        let pkgs = nixpkgs.legacyPackages.${system}; in
        rec {
          lib = import ./lib { inherit pkgs; };

          packages = rec {
            publish = pkgs.callPackage ./pkgs/publish { };
            setupfiles = pkgs.callPackage ./pkgs/setupfiles { };
            default = setupfiles;
          };
        }
      );
}
