{ pkgs ? import <nixpkgs> { } }:

{
  build =
    { pname
    , version
    , setupfiles
    , publish
    , src
    , stdenv ? pkgs.stdenvNoCC
    , emacs ? pkgs.emacs
    , tex ? pkgs.texlive.combined.scheme-basic
    }:

    stdenv.mkDerivation rec {
      inherit pname version src;

      buildInputs = [ emacs tex pkgs.coreutils pkgs.gnused ];

      # postPatch = ''
      #   # set -x
      #   cd $src/src
      #   for file in *.org; do
      #       chmod +w "$file"
      #       substituteInPlace "$file"\
      #           --replace "#+SETUPFILE: ./" "#+SETUPFILE: ${setupfiles}/share/setupfiles/"\
      #           --replace "#+setupfile: ./" "#+setupfile: ${setupfiles}/share/setupfiles/"
      #   done
      # '';

      buildPhase = ''
        # export PATH=${emacs}/bin:${tex}/bin:${pkgs.coreutils}/bin
        # set -x
        runHook preBuild
        cd $src
        emacs -Q --script ${publish}/bin/publish
        ls -lah
        ls -lah ./build
        mkdir -p $out
        for file in ./build/*; do
            echo $file
            cp $file $out/
        done
        runHook postBuild
      '';

      # installPhase = ''
      #   runHook preInstall
      #   set -x
      #   ls -lah
      #   cd $src
      #   ls -lah
      #   mkdir -p $out
      #   for file in ./build/*; do
      #       cp $file $out/
      #   done
      #   set +x
      #   runHook postInstall
      # '';
    };
}
